﻿using System;
using System.IO;
using CoreLocation;
using UIKit;

namespace Assigneurdesalles
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            this.numSalleText.ReturnKeyType = UIReturnKeyType.Done;
            this.numSalleText.EnablesReturnKeyAutomatically = true;



            var g = new UITapGestureRecognizer(() => View.EndEditing(true))
            {
                CancelsTouchesInView = false //for iOS5
            };

            View.AddGestureRecognizer(g);

            this.numSalleText.ShouldReturn = (textField) => {
                textField.ResignFirstResponder();
                return true;
            };

            locationActivator();

            // Perform any additional setup after loading the view, typically from a nib.

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void BoutonAssigner_TouchUpInside(UIButton sender)
        {

            CLLocation location = new CLLocation();
            CLLocationManager locationManager = new CLLocationManager();
            locationManager.StartUpdatingLocation();
            this.locationLabel.Text = locationManager.Location.ToString();
            this.stepper.Value = Double.Parse(this.numSalleText.Text);
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.Recent);
            var filename = Path.Combine(documents, "Salles.txt");
            File.AppendAllText(filename, this.numSalleText + ":" + location.Coordinate.Latitude.ToString() + "," + location.Coordinate.Longitude.ToString() + "," + location.Altitude.ToString() + ";");
            Console.WriteLine("file appended");
        }

        partial void valueChanged(UIStepper sender)
        {
            this.numSalleText.Text = stepper.Value.ToString();
        }

        partial void GetSalle_TouchUpInside(UIButton sender)
        {

            }

        public void locationActivator(){
            Console.WriteLine("locActivated1");
            CLLocationManager locationManager = new CLLocationManager();
            locationManager.RequestWhenInUseAuthorization();
            locationManager.StartUpdatingLocation();
            locationManager.AllowsBackgroundLocationUpdates = true;
            bool locActivated = true;
            locationManager.DesiredAccuracy = 0.2;
            Console.WriteLine("locActivated2");
        }
        }
}
