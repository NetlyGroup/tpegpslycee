// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Assigneurdesalles
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton boutonAssigner { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton getSalle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel locationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MapKit.MKMapView mainMap { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField numSalleText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStepper stepper { get; set; }

        [Action ("BoutonAssigner_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BoutonAssigner_TouchUpInside (UIKit.UIButton sender);

        [Action ("GetSalle_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void GetSalle_TouchUpInside (UIKit.UIButton sender);

        [Action ("valueChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void valueChanged (UIKit.UIStepper sender);

        void ReleaseDesignerOutlets ()
        {
            if (boutonAssigner != null) {
                boutonAssigner.Dispose ();
                boutonAssigner = null;
            }

            if (getSalle != null) {
                getSalle.Dispose ();
                getSalle = null;
            }

            if (locationLabel != null) {
                locationLabel.Dispose ();
                locationLabel = null;
            }

            if (mainMap != null) {
                mainMap.Dispose ();
                mainMap = null;
            }

            if (numSalleText != null) {
                numSalleText.Dispose ();
                numSalleText = null;
            }

            if (stepper != null) {
                stepper.Dispose ();
                stepper = null;
            }
        }
    }
}